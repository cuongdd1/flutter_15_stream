import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void printMessage(String message) {
  if (kDebugMode) {
    print(message);
  }
}

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: 'MyHomePage',
      routes: {
        MyHomePage.routeName: (context) => const MyHomePage(),
        Detail.routeName: (context) => const Detail(),
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  static const routeName = 'MyHomePage';
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<int> _numbers = <int>[];

  void getDataSource() async {
    printMessage('start');
    _numbers = await Demo().getNumbers();
    // _numbers.forEach((element) {
    //   printMessage(element.toString());
    // });
    printMessage('done $_numbers');
    setState(() {});
  }

  Widget _tableviewCell(BuildContext context, int index) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            _numbers[index].toString(),
            style: const TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
          IconButton(
              onPressed: () {
                Navigator.pushNamed(context, Detail.routeName,
                    arguments: _numbers[index]);
              },
              icon: const Icon(Icons.arrow_forward),
              color: Colors.white,),
        ],
      ),
    );
  }

  Widget _buildListView(BuildContext context) {
    return ListView.builder(
      itemCount: _numbers.length,
      itemBuilder: (context, index) => Card(
        color: Colors.blueAccent,
        margin: const EdgeInsets.symmetric(vertical: 8),
        child: _tableviewCell(context, index),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Asynchronous programming: Streams'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: _numbers.isEmpty ? const Text('Empty') : _buildListView(context),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          getDataSource();
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class TestStream {
  main() async {
    // Tạo thời gian trễ
    Duration delayTime = const Duration(seconds: 3);
    // Tạo Stream kiểu dữ liệu int, sau delayTime sẽ tạo ra một số trong Stream
    Stream<int> stream = Stream<int>.periodic(delayTime, makeNumber);

    // chờ trong stream và đọc dữ liệu từ stream đó
    await for (int i in stream) {
      printMessage(
          'waiting in stream and read data from that stream $i.toString()');
    }
  }

// value bắt đầu từ 0
  int makeNumber(int value) => (value + 1);
}

class Demo {
  Future<List<int>> getNumbers() async {
    var rng = Random();
    List<int> numbers =
        List<int>.generate(1000000, (int number) => rng.nextInt(1000000));
    numbers.sort((number1, number2) => number1.compareTo(number2));
    return Future.delayed(const Duration(seconds: 4), () => numbers);
  }
}

class Detail extends StatefulWidget {
  static const routeName = 'Detail';
  const Detail({Key? key}) : super(key: key);

  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    final number = ModalRoute.of(context)!.settings.arguments as int;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          number.toString(),
        ),
      ),
    );
  }
}
